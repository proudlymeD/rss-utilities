#!/bin/bash
# Gets RSS link of a YouTube channel

function printhelp (){
  echo -e 'This script returns the RSS feed link for a youtube channel.
  Use -c|--channel to paste channel names, and -u|--url to paste urls.
  Use -h|--help or giving no input returns this help message.\n'
  exit 0
}

if [[ $# -eq 0 ]]; then echo -e "No input command found, displaying help: \n"; printhelp; fi

while [ ! -z "$1" ]; do
case "$1" in 
  -h|--help)
    printhelp;;
  -c|--channel)
    shift; Chname="$1"
    Url="https://www.youtube.com/user/$Chname";;
  -u|--url)
    shift; Url="$1";;
esac
shift
done

curl $Url |\
  sed 's/,/\n/g' |\
  grep 'rssUrl' |\
  sed 's/"//g' |\
  sed 's/rssUrl://'
